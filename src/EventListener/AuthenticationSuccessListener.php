<?php
namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
     $data = $event->getData();
     $user = $event->getUser();
     $data['code'] = $event->getResponse()->getStatusCode();
     $city = $user->getCity();
     $address = null;
     if($city){
         $state = $city->getState();
     $country = $state->getCountry();
     $address = array();
     $address['name'] = $city->getName();
     $address['state']['name'] = $state->getName();
     $address['state']['country']['name'] = $country->getName();
     }
    $data['data'] = array(
        'id' => $user->getId(),
        'username' => $user->getUsername(),
        'email' => $user->getEmail(),
        'addressLocation' => $user->getAddressLocation(),
        'phone' => $user->getPhone(),
        'roles' => $user->getRoles(),
        'ciudad' => $address,
    );
    $event->setData($data);

    }
}
