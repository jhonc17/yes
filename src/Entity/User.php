<?php
namespace App\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Controller\CustomUser;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
 /**
 * User
 * @ORM\Entity
 * @ApiResource( 
 *     attributes={"pagination_enabled"=true,"pagination_client_enabled"=true,"pagination_items_per_page"=20},
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listU"}},},
 *     "create"={"controller"=CustomUser::class,"method"="POST","validation_groups"={"a","b"},"denormalization_context"={"groups"={"newU"}},"normalization_context"={"groups"={"detailU"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"detailU"}}},
 *     "update"={"method"="PUT","validation_groups"={"a"},"controller"=CustomUser::class,"denormalization_context"={"groups"={"updateU"}},"normalization_context"={"groups"={"detailU"}},},
 *     "delete"={"method"="DELETE","controller"=CustomUser::class}
 *   },
 *     )
 * @ApiFilter(BooleanFilter::class, properties={"enabled"})}
 * @ApiFilter(SearchFilter::class, properties={"email": "exact","city.state.country":"exact","city.state"="exact","city"="exact"})
 */
class User extends BaseUser {
    
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"newP"})
     * @Groups({"detailU"})
     * @Groups({"newES","updateES"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="postalZone", type="string", length=255,nullable=true)
     * @Groups({"detailU","newU","updateU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $postalZone;
    
     /**
     * @var string
     *
     * @ORM\Column(name="addressLocation", type="string", length=255,nullable=true)
     * @Groups({"detailU","newU","updateU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $addressLocation;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=255,nullable=true)
     * @Groups({"detailU","newU","updateU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $phone;
    
     /**
     * @var string
     * @ORM\Column(name="fullName", type="string", length=255,nullable=true)
     * @Groups({"detailU","newU","listU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $fullName;
    
     /**
     * @var string
     * @ORM\Column(name="dateRegister", type="datetime",nullable=true)
     * @Groups({"detailU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $dateRegister;
    
    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="users")
     * @Groups({"newU","detailU","updateU"})
     * @Assert\NotBlank(groups={"a"})
     */
    private $city;

    /**
     * @ORM\ManyToMany(targetEntity="EntitySport", mappedBy="users")
     */
    private $entitySports;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addressLocation
     *
     * @param string $addressLocation
     * @return User
     */
    public function setAddressLocation($addressLocation)
    {
        $this->addressLocation = $addressLocation;

        return $this;
    }

    /**
     * Get addressLocation
     * @return string 
     */
    public function getAddressLocation()
    {
        return $this->addressLocation;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
     /**
     * Set enabled
     * @Groups({"newU","updateU"})
     * @param boolean $enabled
     * @return Enabled
     */
     public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     * @return boolean 
     * @Groups({"listU","detailU"})
     * @Assert\NotBlank(groups={"a"})
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    
    /**
     * Get email
     * @return string 
     * @Groups({"listU","detailU"})
     * @Assert\NotNull(groups={"a"})
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->entitySports = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateRegister= new \DateTime('now');
    }

    /**
     * Set city
     *
     * @param \App\Entity\City $city
     * @return User
     */
    public function setCity(\App\Entity\City $city = null)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     * @return \App\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add entitySports
     *
     * @param \App\Entity\EntitySport $entitySports
     * @return User
     */
    public function addEntitySport(\App\Entity\EntitySport $entitySports)
    {
        $this->entitySports[] = $entitySports;
        return $this;
    }

    /**
     * Remove entitySports
     *
     * @param \App\Entity\EntitySport $entitySports
     */
    public function removeEntitySport(\App\Entity\EntitySport $entitySports)
    {
        $this->entitySports->removeElement($entitySports);
    }

    /**
     * Get entitySports
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntitySports()
    {
        return $this->entitySports;
    }
    
     /**
     * {@inheritdoc}
     * @param string $plainPassword
     * @Groups({"newU"})
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        return $this;
    }
    
     /**
     * {@inheritdoc}
     * @param string $plainPassword
     * @Assert\NotBlank(groups={"b"})
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
   
     /**
     * {@inheritdoc}
     * @Groups({"newU","updateU"})
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();
        foreach ($roles as $role){
            $this->addRole($role);
        }
        return $this;
    }
    
     /**
     * {@inheritdoc}
     * @Groups({"detailU"})
     * @Assert\NotBlank(groups={"a"})
     */
    public function getRoles()
    {
        $roles = $this->roles;
        /*foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles|, $group->getRoles());
        }*/
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;
        return array_unique($roles);
    }
    
     /**
     * @Groups({"newU"})
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
    
     /**
     * @Groups({"detailU","listU"})
     *@Assert\NotBlank(groups={"a"})
     */
    public function getUsername()
    {
       return $this->username;
    }
    
     /**
     * @Groups({"newU"})
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getFullName()
    {
       return $this->fullName;
    }
    
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }
    
        public function getPostalZone()
    {
       return $this->postalZone;
    }
    
    public function setPostalZone($postalZone)
    {
        $this->postalZone = $postalZone;
        return $this;
    }
    
      public function getDateRegister()
    {
       return $this->dateRegister->format('d-m-Y');
    }
}