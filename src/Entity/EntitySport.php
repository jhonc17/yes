<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CustomEntitySport;

/**
 *
 * @ORM\Entity
 * Discipline
 * @ApiResource(
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listES"}},},
 *     "post"={"controller"=CustomEntitySport::class,"denormalization_context"={"groups"={"newES"}},"normalization_context"={"groups"={"viewNES"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"detailES"}}},
 *     "put"={"denormalization_context"={"groups"={"updateDiscipline"}},"normalization_context"={"groups"={"detailDiscipline"}},},
 *     "delete"
 *   },
 * )
 */
class EntitySport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"viewNES","detailES"})
     */
    private $id;

     /**
     * @Assert\NotBlank
     * @var string
     * @ORM\Column( type="string", length=255)
     * @Groups({"newES","listES","detailES"})
     */
    private $email;

     /**
     * @Assert\NotBlank
     * @var string
     * @ORM\Column( type="boolean", length=255)
     * @Groups({"newES","listES","detailES"})
     */
    private $enabled;


     /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column( type="string", length=255)
     * @Groups({"newES","listES","detailES"})
     */
    private $address;

     /**
     * @var string
     *
     * @ORM\Column( type="string", length=255)
     * @Groups({"newES","detailES"})
     * @Assert\NotBlank
     */
    private $zipCode;

     /**
     * @var string
     *
     * @ORM\Column( type="string", length=255)
     * @Groups({"newES","detailES"})
     * @Assert\NotBlank
     */
    private $phone;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"newES","detailES","listES"})
     * @Assert\NotBlank
     */
    private $name;

     /**
     * @ORM\ManytoOne(targetEntity="City", inversedBy="entitiesSports")
     * @Groups({"newES","detailES"})
     * @Assert\NotBlank
     */
    protected $city;

    /**
     * @Groups({"newES"})
     * @ORM\ManytoMany(targetEntity="User", inversedBy="entitySports")
     * @Assert\NotBlank
     */
    protected $users;

     /**
     * @ORM\OneToMany(targetEntity="Dependence", mappedBy="entitySport", cascade={"persist"})
     * @Groups({"newES"})
     */
    private $dependences;

     /**
     * @Groups({"newES"})
     * @ORM\ManyToMany(targetEntity="Discipline", inversedBy="entitySports")
     */
    protected $disciplines;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }


      public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

     public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }


     public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->disciplines = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dependences = new \Doctrine\Common\Collections\ArrayCollection();
    }

   public function addUser(\App\Entity\User $users)
    {
        $this->users[] = $users;
        return $this;
    }

    /**
     * Remove users
     *
     * @param \App\Entity\User $users
     */
    public function removeUser(\App\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

 /**
     * Set city
     *
     * @param \App\Entity\City $city
     * @return EntitySport
     */
    public function setCity(\App\Entity\City $city = null)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return \App\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add disciplines
     *
     * @param \App\Entity\Discipline $disciplines
     * @return EntitySport
     */
    public function addDiscipline(\App\Entity\Discipline $disciplines)
    {
        $this->disciplines[] = $disciplines;
        return $this;
    }

    /**
     * Remove disciplines
     *
     * @param \App\Entity\Discipline $disciplines
     */
    public function removeDiscipline(\App\Entity\Discipline $disciplines)
    {
        $this->disciplines->removeElement($disciplines);
    }

    /**
     * Get disciplines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDisciplines()
    {
        return $this->disciplines;
    }

    /**
     * Add dependence
     *
     * @param \App\Entity\Dependence $dependences
     * @return EntitySport
     */
    public function addDependence(\App\Entity\Dependence $dependence)
    {
        $dependence->setEntitySport($this);
        $this->dependences[] = $dependence;
        return $this;
    }

    /**
     * Remove dependence
     *
     * @param \App\Entity\Dependence $dependence
     */
    public function removeDependence(\App\Entity\Dependence $dependence)
    {
        $this->dependences->removeElement($dependence);
        $dependence->setEntitySport(null);
    }   

    /**
     * Get dependences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDependences()
    {
        return $this->dependences;
    }

}
