<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * country
 * @ApiResource(
 *     collectionOperations={"get"={"normalization_context"={"groups"={"country"}},},
 *     "post"={"denormalization_context"={"groups"={"pCountry"}},"normalization_context"={"groups"={"idCountry"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"country"}}},
 *     "put"={"denormalization_context"={"groups"={"pCountry"}},"normalization_context"={"groups"={"idCountry"}},},
 *     "delete"
 *   },
 *     )
 * @ApiFilter(BooleanFilter::class, properties={"enabled"})
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"idCountry","country"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @Groups({"country","pCountry"})
     */
    private $name;

    /**
     * @var boolean
     * @Assert\NotNull
     * @ORM\Column(name="enabled", type="boolean", length=255,nullable=true)
     * @Groups({"country","pCountry"})
     */
    private $enabled;

    /**
     *
     * @ORM\OneToMany(targetEntity="State", mappedBy="country")
     * @ApiSubresource
     */
    private $states;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param string $enabled
     * @return country
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return string
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->states = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add states
     *
     * @param \App\Entity\State $states
     * @return country
     */
    public function addState(\App\Entity\State $states)
    {
        $this->states[] = $states;

        return $this;
    }

    /**
     * Remove states
     *
     * @param \App\Entity\State $states
     */
    public function removeState(\App\Entity\State $states)
    {
        $this->states->removeElement($states);
    }

    /**
     * Get states
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStates()
    {
        return $this->states;
    }
}
