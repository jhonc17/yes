<?php
namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * City
 * @ApiResource(
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listCity"}},},
 *     "api_states_cities_get_subresource"={"method"="GET", "normalization_context"={"groups"={"listCity"}},},
 *     "api_countries_states_cities_get_subresource"={"method"="GET", "normalization_context"={"groups"={"listCity"}},},
 *     "post"={"denormalization_context"={"groups"={"pCity"}},"normalization_context"={"groups"={"idCity"}},}
 * },
 *     itemOperations={"get",
 *     "put"={"denormalization_context"={"groups"={"pCity"}},"normalization_context"={"groups"={"idCity"}},},
 *     "delete"
 *   },
 *     )
 * @ApiFilter(BooleanFilter::class, properties={"enabled"})
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="city")
 * @ORM\Entity
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"listCity","idCity","detailCity"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="name", type="string", length=255 ,nullable=true)
     * @Groups({"listCity","detailCity","pCity"})
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean",nullable=true)
     * @Assert\NotNull
     * @Groups({"listCity","detailCity","pCity"})
     */

    private $enabled;

    /**
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="city")
     */
    private $users;

    /**
     *
     * @ORM\OneToMany(targetEntity="EntitySport", mappedBy="city")
     */
    private $entitiesSports;

    /**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="cities")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotNull
     * @Groups({"detailCity","pCity"})
     *
     */
    private $state;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return City
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entitiesSports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add users
     *
     * @param \App\Entity\User $users
     */
    public function addUser(\App\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \App\Entity\User $users
     */
    public function removeUser(\App\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add entitiesSport
     *
     * @param \App\Entity\EntitySport $entitiesSport
     * @return City
     */
    public function addEntitiesSport(\App\Entity\EntitySport $entitiesSport)
    {
        $this->entitiesSports[] = $entitiesSport;

        return $this;
    }

    /**
     * Remove entitiesSport
     *
     * @param \App\Entity\EntitySport $entitiesSport
     */
    public function removeEntitiesSport(\App\Entity\EntitySport $entitiesSport)
    {
        $this->entitiesSport->removeElement($entitiesSport);
    }

    /**
     * Get entitiesSport
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntitySport()
    {
        return $this->entitiesSports;
    }

    /**
     * Set state
     *
     * @param \App\Entity\State $state
     * @return City
     */
    public function setState(\App\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \App\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }

 /**
 * @ORM\PreRemove
 */
public function nullUsers()
{
    foreach ($this->users as $user) {
        $user->setCity(null);
    }
    foreach ($this->entitiesSport as $entitySport) {
        $entitySport->setCity(null);
    }
}
}
