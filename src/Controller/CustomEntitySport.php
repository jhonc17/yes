<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\EntitySport;
class CustomEntitySport extends Controller
{
    public function __invoke(EntitySport $data,Request $request)
    {

      if (Request::METHOD_POST == $request->getMethod()){
           $em = $this->getDoctrine()->getEntityManager();
           $dependenciesReferenced = $data->getDependences();
           $dependencesClone = new \Doctrine\Common\Collections\ArrayCollection();
                foreach ($dependenciesReferenced as $value){
                    $dependencesClone[] = clone $value;
                }
           $dependenciesReferenced->clear();
           $em->flush();
                foreach ($dependencesClone as $value){
                    $data->addDependence($value);
                }
           $em->persist($data);
           $em->flush();
           $response['title'] = "Successfull";
           $response['detail'] = "EntitySport deleted";
           $status = 200;
           return new JsonResponse($response,$status);
       } 
        
       if (Request::METHOD_DELETE == $request->getMethod()){
           $em = $this->getDoctrine()->getEntityManager();
           $entitySport = $em->getRepository("App:EntitySport")->find($data->getId());
           $response = array();
           $entitySport->setEnabled(FALSE);
           $response['title'] = "Successfull";
           $response['detail'] = "EntitySport deleted";
           $status = 200;
           return new JsonResponse($response,$status);
       }
}
}
