<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Discipline;
class CustomDiscipline extends Controller
{
    public function __invoke(Discipline $data,Request $request)
    {
       if (Request::METHOD_DELETE == $request->getMethod()){
           $em = $this->getDoctrine()->getEntityManager();
           $discipline = $em->getRepository("App:Discipline")->find($data->getId());
           $response = array();
          try {
              $em->remove($discipline);
              $em->flush();
              $response['title'] = "Successfull";
              $response['detail'] = "Discipline deleted";
              $status = 200;
          } catch (Exception $ex){
             $response['title'] = "An error occurred";
             $response['detail'] = "Discipline not deleted";
             $status = 403;
          }
               return new JsonResponse($response,$status);
       }
}
}
