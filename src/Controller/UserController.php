<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;

class UserController extends Controller
{
    /**
     * @Route("/admin/register", name="register_user",methods="POST")
     */
    public function number(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');
        $enabled = $request->get('enabled');
        $user = new User();
        $user->setEmail($email);
        $user->setIsActive($enabled);
        $user->setPassword($password);
        $user->setUsername($username);
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($user);
        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        return new Response(
            '<html><body>Username: '.$username.'</body></html>'
        );
    }
    
    /**
     * @Route("/api/users/changePassword", name="change-password",methods="POST")
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $encoder) {
        if($request->getMethod() == 'POST') {
        $content = $request->getContent();
        $content = json_decode($content,true);
        $old_pwd = $content['oldPassword'];
        $new_pwd = $content['newPassword'];
        $new_pwd1 =$content['newPasswordRepeat'];
        if($new_pwd!=$new_pwd1){ 
            return new \Symfony\Component\HttpFoundation\JsonResponse(["title"=>"Error",'detail'=>'Contraseñas no Coinciden'],400);
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $valid = $encoder->isPasswordValid($user,$old_pwd);
        if(!$valid) {
            return new \Symfony\Component\HttpFoundation\JsonResponse(['title'=>'Error','detail'=>'Contraseña Antigua No coincide'],400);
        }else {
            $new_pwd_encoded = $encoder->encodePassword($user, $new_pwd);
            $user->setPassword($new_pwd_encoded);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();
            return new \Symfony\Component\HttpFoundation\JsonResponse(['title'=>'Successfull','detail'=>'Cotraseña cambiada']);
        }
    }
    }
    
    /**
     * @Route("/api/recoveryPassword", name="recovery-password",methods="POST")
     */
    public function recoveryPasswordAction(\Swift_Mailer $mailer, Request $request,TokenGeneratorInterface $tokenGenerator)
    {
    $em = $this->getDoctrine()->getEntityManager();
    $content = $request->getContent();
    $content = json_decode($content,true);
    $mail = $content['email'];
    $user = $em->getRepository('App:User')->findOneByEmail($mail);
    $response = array();
    
    if($user){
     $token = $tokenGenerator->generateToken();
     $user->setConfirmationToken($token);
     $em->flush();
        try {
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('jh0n.h3nryy@gmail.com')
        ->setTo('jh0n.h3nryy@gmail.com')
        ->setBody($this->renderView('recoveryPassword.html.twig',array('data'=>$token)),'text/html');
        $mailer->send($message);
        $response= ["title"=>"Successful","Detail"=>"Si el correo existe el mensaje fue enviado"];
        $status = 200;
         } catch (Exception $exc) {
        $response= ["title"=>"Error","Detail"=>"No se pudo enviar el mensaje"];
        }
        }else{
        $response= ["title"=>"Error","Detail"=>"No existe email"];   
         $status = 400;
        }
     return new \Symfony\Component\HttpFoundation\JsonResponse($response,$status);
}

    /**
     * @Route("/api/recoveryPasswordVerification", name="recovery-password-verification",methods="POST")
     */
    public function recoveryPasswordVerificationAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
    $em = $this->getDoctrine()->getEntityManager();
    $content = $request->getContent();
    $content = json_decode($content,true);
    $password= $content['password'];  
    $validationPassword=$content['validationPassword'];  
    
    $response = array();
    if($password == $validationPassword){
        $token = $content['token'];

        $user = $em->getRepository('App:User')->findOneByConfirmationToken($token);
               if($user){
                $new_pwd_encoded = $encoder->encodePassword($user, $password);
                $user->setPassword($new_pwd_encoded);                   
                $em->flush();
               $response= ["title"=>"Successful","Detail"=>"Contraseña Cambiada"];   
                $status = 200;    
               }else{
               $response= ["title"=>"Error","Detail"=>"No existe solicitud"];   
                $status = 400;
               }
    }else{
        
     $response= ["title"=>"Error","Detail"=>"Las contraseñas no coinciden"];   
     $status = 400;
    }
    return new \Symfony\Component\HttpFoundation\JsonResponse($response,$status);
}
}
