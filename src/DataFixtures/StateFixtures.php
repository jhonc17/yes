<?php
namespace App\DataFixtures;
use App\Entity\State;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\CountryFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class StateFixtures extends Fixture implements DependentFixtureInterface
{
 
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 10; $i++) {
            $state = new State();
            $state->setName('State '.$i);
            $state->setEnabled(TRUE);
            $state->setCountry($this->getReference('country '.mt_rand(0,5)));
            $this->addReference('state '.$i, $state);
            $manager->persist($state);
        }
                $manager->flush();

    }
    
          public function getDependencies(){
        return array(
        CountryFixtures::class,
        );
    }
}
