<?php
namespace App\DataFixtures;
use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\StateFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CityFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager) 
    {
        for ($i = 0; $i <= 20; $i++) {
            $city = new City();
            $city->setName('City '.$i);
            $city->setEnabled(FALSE);
            $city->setState($this->getReference('state '.mt_rand(0,10)));
            $this->addReference('city '.$i, $city);
            $manager->persist($city);
        }
        $manager->flush();
    }
    
      public function getDependencies()
    {
        return array(
        StateFixtures::class,
        );
    }
}
